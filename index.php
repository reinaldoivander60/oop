<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$hewan = new animal("Shaun");

echo "Name : ". $hewan->nama. "<br>";
echo "Legs : ". $hewan->legs . "<br>";
echo "Cold Blooded : ". $hewan->cold_blooded . "<br> <br>";

$kodok = new frog("buduk");

echo "Name : ". $kodok->nama. "<br>";
echo "Legs : ". $kodok->legs . "<br>";
echo "Cold Blooded : ". $kodok->cold_blooded . "<br>";
echo "Jump : ". $kodok->gerak(). "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Name : ". $sungokong->nama. "<br>";
echo "Legs : ". $sungokong->legs . "<br>";
echo "Cold Blooded : ". $sungokong->cold_blooded . "<br>";
echo "Yell : ". $sungokong->yell(). "<br><br>";

?>